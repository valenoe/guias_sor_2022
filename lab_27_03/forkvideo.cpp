#include <iostream> 
using namespace std;


#include <unistd.h>
#include <sys/wait.h>


#include "forkvideo.h"

Fork::Fork(int seg, char *url, char *nombre){
	
	name = nombre;
	segundos = seg;
	URL = url;
	crear();
	ejecutarCodigo();
}


void Fork::crear(){
	pid = fork();
}
void Fork::ejecutarCodigo(){
	// valida la creación de proceso hijo.
    if (pid < 0) {
		cout << "No se pudo crear el proceso ...";
        cout << endl;
      
	} else if (pid == 0) {
        // Código del proceso hijo.
        cout << "ID Proceso hijo: " << getpid();
        cout << endl;
        cout << endl;
        
        cout << "Ejecuta código proceso hijo ...";
        cout << endl;
        
        /** en esta zona execlp va a ejecutar la aplicación youtube-dl
         * con los parametros:
         * 		-x para descargar solo el audio
         * 		--restrict-filenames --output name para guardar el audio con el nombre registrado en el puntero name
         * 		--audio-format mp3 para guardar el audio solo con el formato mp3
         * 		y URL es el puntero que contiene la dirección web del video
         * */
    
        
		cout<<"Descargando video y extrayendo audio"<<endl;
		execlp("youtube-dl", "youtube-dl", "-x", "--restrict-filenames", "--output", name, "--audio-format", "mp3", URL, NULL);   
        
        cout << endl;
        cout << endl;
        
        
        // Espera "segundos" para continuar
        sleep(segundos);
	
	} else {
        // Código proceso padre.
        // Padre espera por el término del proceso hijo.
        wait (NULL);

        cout << "Termina código de proceso hijo ...";
        cout << endl;
        cout << "Continua con código proceso padre: " << getpid();
        
        /** Ya en el proceso padre se reproduce el video directamente en vlc gracias al nombre
         * guardado en el puntero name*/
        cout<<"\nReproducir..."<<endl;
		execlp("vlc", "vlc", name, NULL);   
		
		
		// Otro camino (fallido)
		//execlp("find", "find", ".", "-name", "'*.mp3'", NULL);
        cout << endl;
	}

}
