#include <iostream> 
using namespace std;


#include <unistd.h>
#include <sys/wait.h>
#include <ctime>
#include <stdlib.h>


#include "forkvideo.h"




int main(int argc, char **argv){
	if(argc!=2){
		cout << "Comando mal ejecutado, el programa no se podrá inciar" << endl;
		cout << "Recuerde que el programa recibe 2 parametros de entrada, de la siguiente forma : " << endl;
		cout << "	./programa URL "<<endl;
		cout << "\n";
		exit(-1);
	}
	else{
		// Punteros para guardar URL y el nombre del archivo
		char *dato_url, *dato_nombre;
		
		// argv[1] almacena el URL ya que se ingresa al principio por terminal
		dato_url = argv[1];
		
		
		/** Se crea una cadena de carcateres aleatorios con terminación .mp3
		 * esto se va a guardar en el puntero dato_nombre para modificar el 
		 * nombre del audio descargado con el fin de abrir el archivo de audio
		 * directo desde el programa, la aleatoriedad de las letras también sirve porque
		 * no se pueden descargar dos archivos con el mismo nombre, no se reescribe, 
		 * yputube-dl simplemente ignora la nueva descarga 
		 * 
		 * Finamente se imprime el nuevo nombre y se comienzan los procesos*/
		char nombre[10];
		srand(time(NULL));
		for(int i = 0; i<6; i++){
			nombre[i] = 64+rand()%(123-64);
		}
		nombre[6] = '.';
		nombre[7] = 'm';
		nombre[8] = 'p';
		nombre[9] = '3';
		cout<<"nombre del archivo a descargar: "<<nombre<<endl;
		cout<<endl;
		dato_nombre = nombre;
		
		
		Fork miFork(1, dato_url, dato_nombre);
		
		
		
  }
  return 0;
  
 }
