

#ifndef FORKVIDEO_H 
#define FORKVIDEO_H

#include <unistd.h>
#include <sys/wait.h>
#include <iostream>
using namespace std;

class Fork {
	
	private:
        pid_t pid;
        int segundos;
        char *URL;
        char *name; // nombre que va a obtener el video
        
    public:
		Fork(int seg, char *url, char *nombre);
		void crear();
		void ejecutarCodigo();
   
};

#endif

