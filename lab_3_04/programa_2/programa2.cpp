


#include <iostream>
using namespace std;

#include <fstream>
#include "programa2.h"

programa2::programa2(){
}

void programa2::lineas_y_caracteres(datos_t *dato){
	
	// se abre el archivo con nombre fe
	ifstream fe(dato->nombre_archivo);

	string linea;
	int cont_linea=0, cont_car= 0;

  
	while(getline(fe, linea)){
		/** el ciclo va a correr cada vez que haya una linea (al final del 
		* archivo se detiene) y va a aumnetar el contador de lineas.
		* 
		* el contendo de la linea se guarda en la variable linea, entonces 
		* se recorre esta variable para saber la cantidad de 
		* carcacteres  y se guarda en un contador
		* */
		cont_linea++;
		
		for(int i=0; i<linea.size(); i++){
			cont_car++;
		}
		
	}
	
	/* Se guarda la cantidad de lineas y carcateres en la estructura*/
	dato->lineas = cont_linea;
	dato->caracteres = cont_car;
 
	palabras(dato);
	
	
}

void programa2::palabras(datos_t *dato){
	
	// Cadena para guardar las palabras (una por una)
	string cadena;
	// se abre el archivo con nombre fe
	ifstream fe(dato->nombre_archivo);
	int cont_palabras=0;
	

	
	while (!fe.eof()) {
		/* Mientras el archivo no llegue a su final
		 * va a pasar cada palabra que encuentre a la variable cadena
		 * Cuando pase esto va a aumentar el contador*/
		fe >> cadena;
		cont_palabras++;

	}
	
	fe.close();
	dato->palabras = cont_palabras;
	
	
  
}

