Segundo programa

Función: contar lineas, cracteres y palabras de los textos entregados, sumar datos.

Diferencia: uso de hebras y clases.

Para abrir el programa debe ejecutar el comando make, luego el nombre del programa (contador2) y los nombrres de los archivos que quiera contar.

ej: 	> make
	> ./contador2 texto1.txt texto2.txt ... 
	
El programa debe recibir si o si 2 parámetros o más, de no hacerlo, se cerrará.

Ya dentro del porgrama, mediante una estructura, se iniciará tomando el nombre de cada archivo, para que con las funciones de lines_y_carcateres() y palabras() se obtengan las cantidades de lineas, caractere y palabras.

Se definirá una estructura para cada archivo dentro de un vector, estos datos al final se sumarán y se mostrá 
el resultado.
