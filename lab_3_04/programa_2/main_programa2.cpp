
#include <iostream>
using namespace std;
#include <pthread.h>
#include <fstream>
#include <iostream>
#include <ctime> 
#include "programa2.h"


void *hebra_principal(void *archivo){
	/** Hebra que inicia la clase que se encarga de contar caracteres, 
	 * lineas y palabras
	 * 
	 * recibe a archvo el cual tienne que convertir a datos_t
	 * 
	 * datos_t es una estructura que guardará el nombre del archivo, 
	 * la cantidad de lineas, de caracteres y de palabras
	 * */
	datos_t *arch;
	arch = (datos_t *) archivo;
	programa2 p2;
	
	p2.lineas_y_caracteres(arch);
	pthread_exit(0);
}

int main(int argc, char *argv[]) {
	
	if(argc < 2){
		cout << "Comando mal ejecutado, el programa no se podrá inciar" << endl;
		cout << "Recuerde que el programa recibe dos o más parametros de entrada, de la siguiente forma : " << endl;
		cout << "	./contador texto1.txt texto2.txt ... "<<endl;
		cout << "\n";
		exit(-1);
	}
	else{
	
		// variables para contar el tiempo de ejecución
		unsigned t0, t1;
		t0=clock();
	
		// una hebra por cada archvio
		pthread_t threads[argc - 1];
		
		// una estructura por cada archivo
		datos_t datos[argc -1]; 
		int i = 0;
		

		int total_lineas = 0;
		int total_caracteres = 0;
		int total_palabras = 0;
		
	
		/**	inicializa las estructuras, con el nombre del archivo y 
		 * los demás datos en 0 
		 * 
		 * Crea todos los hilos 
		 * Por cada texto se crea una hebra */
		for (i=0; i < argc - 1; i++) {
			datos[i].nombre_archivo = argv[i+1];
			datos[i].lineas = 0;
			datos[i].caracteres = 0;
			datos[i].palabras= 0;
			pthread_create(&threads[i], NULL, hebra_principal, &datos[i]);
		}
	
		/** Para esperar por el término de todos los hilos 
		* para que el procesos continue su ejecucion tiene qu esperar 
		* que todas las hebras se ejecuten*/
		for (i=0; i< argc - 1; i++) {
			pthread_join(threads[i], NULL);
		}
		
		
		for(i=0; i < argc - 1; i++){
			cout<<endl;
			cout<<"Archivo: "<<datos[i].nombre_archivo<<endl;
			cout<<endl;
			cout<<"lineas: "<<datos[i].lineas<<endl;
			cout<<"caracteres: "<<datos[i].caracteres<<endl;
			cout<<"oalabras: "<<datos[i].palabras<<endl;
			
			
			total_lineas += datos[i].lineas;
			total_caracteres += datos[i].caracteres;
			total_palabras += datos[i].palabras;
			
		}
		
		cout<<endl;
		cout<<"Total lineas: "<<total_lineas<<endl;
		cout<<"Total caracteres: "<<total_caracteres<<endl;
		cout<<"Total palabras: "<<total_palabras<<endl;
		
		t1 = clock();
	
		// calculo final del tiempo
		double time = (double(t1-t0)/CLOCKS_PER_SEC);
		cout << "Execution Time: " << time <<" segundos"<< endl;
	}
	return 0;
}
