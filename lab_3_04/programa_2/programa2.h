
#ifndef POGRAMA2_H 
#define POGRAMA2_H


#include <iostream>
using namespace std;

typedef struct{
	/**La estructura guarda todos los datos de un archivo
	 * para obtener la suma de todos los archivos al final */
	string nombre_archivo;
	int lineas;
	int caracteres;
	int palabras;
}datos_t;

class programa2 {
	// clase hecha solo para contar

    public:
		programa2();
		void lineas_y_caracteres(datos_t *dato);
		void palabras(datos_t *dato);
   
};

#endif
