
#ifndef POGRAMA1_H 
#define POGRAMA1_H


#include <iostream>
using namespace std;

class programa1 {
	// clase hecha solo para contar

	private:
		int *totales;
		
    public:
		programa1();
		void lineas_y_caracteres(string archivo);
		int palabras(string archivo);
		
		void imprimir();
		
		void inicio_tot();
   
};

#endif
