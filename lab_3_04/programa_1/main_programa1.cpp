
#include <iostream>
#include <ctime> 
#include <iostream>
using namespace std;

#include <fstream>
#include "programa1.h"

int main(int argc, char *argv[]) {
	
	if(argc < 2){
		cout << "Comando mal ejecutado, el programa no se podrá inciar" << endl;
		cout << "Recuerde que el programa recibe dos o más parametros de entrada, de la siguiente forma : " << endl;
		cout << "	./contador texto1.txt texto2.txt ... "<<endl;
		cout << "\n";
		exit(-1);
	}
	else{
		// variables para contar el tiempo de ejecución
		unsigned t0, t1;
 
		t0=clock();
	
	
		// Inicio clase programa1
		programa1 p1;
		
		// inicio de variables en 0 (lineas, caracteres, contador)
		p1.inicio_tot();
		
	
	
		for(int i=1; i<argc; i++){
			/**este ciclo va a llevar cada archivo (uno por uno) a la función 
			* lineas_y_caracteres que cuenta las lineas y caracteres del archivo
			* al final la función dirige el archivo a otra funcón que cuenta las palabras
			* */
			cout<<"\nArchivo "<<i<<" "<<argv[i]<<endl; 
			cout<<endl;
			p1.lineas_y_caracteres(argv[i]);
			
		
		}
		
		
		//imprime el total de datos
		p1.imprimir();
		
	 
		t1 = clock();
		// calculo final del tiempo
		double time = (double(t1-t0)/CLOCKS_PER_SEC);
		cout << "Execution Time: " << time << endl;
	}
	
	return 0;
}
