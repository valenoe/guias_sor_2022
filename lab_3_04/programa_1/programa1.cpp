
#include <iostream>
using namespace std;

#include <fstream>
#include "programa1.h"

programa1::programa1(){
}

void programa1::lineas_y_caracteres(string archivo){

	// se abre el archivo con nombre fe
	ifstream fe(archivo);

	string linea;
	int cont_linea=0, cont_car= 0, cont_palabras = 0;

  
	while(getline(fe, linea)){
		/** el ciclo va a correr cada vez que haya una linea (al final del 
		* archivo se detiene) y va a aumnetar el contador de lineas.
		* 
		* el contendo de la linea se guarda en la variable linea, entonces 
		* se recorre esta variable para saber la cantidad de 
		* carcacteres  y se guarda en un contador
		* */
		
		
		cont_linea++;
		
		for(int i=0; i<linea.size(); i++){
			cont_car++;
		}
		
	}
 
	// Se imorimen los contadores
	cout<<"Cantidad de lineas: "<<cont_linea<<endl;
	cout<<"Cantidad de caracteres: "<<cont_car<<endl;
	
	cont_palabras = palabras(archivo);
	
	
	// Suma a cada variable del puntero
	totales[0] += cont_linea;
	totales[1] += cont_car; // caracteres
	totales[2] += cont_palabras;
	
	
}

int programa1::palabras(string archivo){
	
	// Cadena para guardar las palabras (una por una)
	string cadena;
	// se abre el archivo con nombre fe
	ifstream fe(archivo);
	int cont_palabras=0;
	

	
	while (!fe.eof()) {
		/* Mientras el archivo no llegue a su final
		 * va a pasar cada palabra que encuentre a la variable cadena
		 * Cuando pase esto va a aumentar el contador*/
		fe >> cadena;
		cont_palabras++;

	}
	
	fe.close();
	cout<<"Cantidad de palabras: "<<cont_palabras<<endl;
	return cont_palabras;
  
}


void programa1::imprimir(){
	
	
	cout<<"\n\ntotal de lineas: "<<totales[0]<<endl;
	cout<<"total de Caracteres: "<<totales[1]<<endl;
	cout<<"total de palabras: "<<totales[2]<<endl;
	
}


void programa1::inicio_tot(){
	totales[0] = 0;
	totales[1] = 0;
	totales[2] = 0;
}
